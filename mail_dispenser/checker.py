import pandas as pd
import os
import logging
from typing import Union

token_file = 'path/to/token/file'

mappings_column_names = {'members': {'nr': 'Mitgliedernummer', 'Nachname': 'Name'},
                         'postal_votes': {'name': 'Name', 'vorname': 'Vorname',
                                          'dpg-mitgliedsnummer': 'Mitgliedernummer', 'replyto': 'E-Mail'},
                         'registrations': {'name': 'Name', 'vorname': 'Vorname',
                                           'dpg-mitgliedsnummer': 'Mitgliedernummer', 'replyto': 'E-Mail'},
                         'registrations_online': {'name': 'Name', 'vorname': 'Vorname',
                                                  'dpg-mitgliedsnummer': 'Mitgliedernummer', 'replyto': 'E-Mail'}
                         }
mode = None
debug = False
data = None


def get_debug() -> bool:
    return debug


def set_debug(debug_: bool):
    global debug
    debug = debug_


def get_mode() -> Union[str, None]:
    return mode


def set_mode(mode_: Union[str, None]):
    global mode
    assert mode_ in ['postal', 'reminder', None]
    mode = mode_


def load_data():
    global token_file

    loader_funcs = {'xlsx': pd.read_excel, 'json': pd.read_json, 'csv': pd.read_csv}
    data = {}
    if get_debug():
        print('Gefundene Dateien:')
    for filename in os.listdir("data"):
        name, extension = filename.split(".")
        if get_debug():
            print(f"data/{filename}")
        frame = loader_funcs[extension](f"data/{filename}")
        if name == 'tokens':
            token_file = f"data/{filename}"
        if name == 'no_members':
            continue
        data[name] = frame
    return data


def clean_member_numbers(number: str):
    if not str(number)[-1] == "-":
        return f"{int(number)}-"
    return number


def get_renamed_frame(key: str):
    clean = data[key].rename(mappings_column_names[key], axis='columns')

    # Remove empty rows and columns
    clean = clean.dropna(how='all')
    clean = clean.dropna(how='all', axis=1)
    clean['Mitgliedernummer'] = clean['Mitgliedernummer'].apply(clean_member_numbers)
    return clean


def add_tokens(num_postal_voters: int, num_regular_voters: int, postal_voters: pd.DataFrame,
               voting_members: pd.DataFrame):

    tokens = data['tokens']
    # Shuffle tokens
    tokens = tokens.sample(frac=1).reset_index(drop=True)
    postal_voters = postal_voters.reset_index(drop=True)
    voting_members = voting_members.reset_index(drop=True)

    if get_mode() == 'postal':
        # Tokens for postal voters
        tokens_postal = tokens.iloc[:num_postal_voters]
        tokens = tokens.iloc[num_postal_voters:]
        postal_voters = pd.concat([postal_voters, tokens_postal], axis=1)

        if not get_debug():
            # Save not used tokens
            tokens.to_csv(token_file, index=False)

    else:
        tokens = tokens.iloc[:num_regular_voters]
        voting_members = pd.concat([voting_members, tokens], axis=1)

    return voting_members, postal_voters


def check_registered(member_table, postal_voters, registrations_table):

    postal_voters['postal'] = True
    registrations_table['postal'] = False

    all_potential_participants = pd.concat([postal_voters, registrations_table]).reset_index(drop=True)
    all_potential_participants = all_potential_participants.drop_duplicates(['Mitgliedernummer', 'Name', 'Vorname'])

    joined_frame = all_potential_participants.merge(member_table[['Mitgliedernummer', 'Name', 'Vorname']],
                                             on=['Mitgliedernummer', 'Name', 'Vorname'], how='left', indicator=True)

    # Identify registered persons, that are jDPG members
    joined_frame['registered_member'] = joined_frame['_merge'] == 'both'
    joined_frame.drop(columns=['_merge'], inplace=True)

    # Filter postal voters
    registered_members = joined_frame[joined_frame['registered_member']]
    postal_voters = registered_members[registered_members['postal']]
    voting_members = registered_members[~registered_members['postal']]

    # Identify those, who registered but are not in the member list
    no_members = joined_frame[~joined_frame['registered_member']]

    # Join with member table on member number, where possible
    no_members = no_members.merge(member_table[['Mitgliedernummer', 'Name', 'Vorname']],
                                  on='Mitgliedernummer', suffixes=(None, '_Mitgliederliste'), how='left')

    return voting_members, no_members, postal_voters


def clean_frames():
    # Rename columns
    columns = ['Name', 'Vorname', 'Mitgliedernummer', 'E-Mail']

    # Put every DataFrame into a separate variable
    postal_voters = get_renamed_frame('postal_votes')[columns]
    member_table = get_renamed_frame('members')[columns[:-1]]
    registrations_table = get_renamed_frame('registrations')[columns]

    # Just, use this fragment, if online registration are possible
    if len(data.keys()) == 5:
        registrations_online_table = get_renamed_frame('registrations_online')[columns]
        registrations_table = pd.concat([registrations_table, registrations_online_table], axis=0) \
            .drop_duplicates(columns[:-1])
    return member_table, postal_voters, registrations_table


def split_later_approved():
    no_members_ = pd.read_excel('data/no_members.xlsx')

    # Clean up
    no_members_ = no_members_.dropna(how='all')
    no_members_ = no_members_.dropna(how='all', axis=1)

    # Set correct Datatype
    no_members_ = no_members_.astype({'postal': bool, 'registered_member': bool})

    no_members = no_members_[~no_members_['registered_member']]
    registered_members_ = no_members_[no_members_['registered_member']]
    postal_voters = registered_members_[registered_members_['postal']]
    voting_members = registered_members_[~registered_members_['postal']]
    return voting_members, no_members, postal_voters


def get_frames():
    global data
    data = load_data()

    if not get_mode() == 'reminder':
        voting_members, no_members, postal_voters = check_registered(*clean_frames())
    else:
        voting_members, no_members, postal_voters = split_later_approved()

    num_postal_voters, num_regular_voters = postal_voters.shape[0], voting_members.shape[0]
    logging.info(f"{num_postal_voters + num_regular_voters} tokens required.")
    logging.warning(f"{no_members.shape[0]} not members of jDPG.")

    if not get_mode() == 'reminder':
        voting_members, postal_voters = add_tokens(num_postal_voters,
                                                   num_regular_voters, postal_voters, voting_members)
    else:
        set_mode('postal')
        _, postal_voters = add_tokens(num_postal_voters,
                                      num_regular_voters, postal_voters, voting_members)
        set_mode(None)
        voting_members, _ = add_tokens(num_postal_voters,
                                       num_regular_voters, postal_voters, voting_members)

    return voting_members, no_members, postal_voters
