from mail_dispenser import checker, sender
import argparse
import pandas as pd
import logging


def send_regular_mails(voting_members: pd.DataFrame):
    invitations_dispenser = sender.MailDispenser(voting_members, 'text_files/wahlunterlagen.txt')
    invitations_dispenser.broadcast('Wahlunterlagen für die Wahl des jDPG-Bundesvorstandes')

    return True


def send_postal_voter_mails(postal_voters: pd.DataFrame):
    postal_dispenser = sender.MailDispenser(postal_voters, 'text_files/wahlunterlagen.txt')
    postal_dispenser.broadcast('Deine Briefwahlunterlagen für die Wahl des jDPG-Bundesvorstandes')

    return True


def send_reminder_mails(no_members: pd.DataFrame):
    membership_request_dispenser = sender.MailDispenser(no_members,
                                                        'text_files/aufforderung_mitgliedschaft.txt')
    membership_request_dispenser.broadcast('Deine Anmeldung zur Mitgliederversammlung der jDPG')

    return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Sending mails with invitation links.')
    parser.add_argument('--postal', dest='postal_votes', action='store_true', help='Activate postal vote mode')
    parser.set_defaults(postal_votes=False)
    parser.add_argument('--reminder', dest='reminder', action='store_true', help='Activate reminder mode')
    parser.set_defaults(reminder=False)
    parser.add_argument('--debug', dest='debug', action='store_true', help='Activate debug mode')
    parser.set_defaults(debug=False)
    args = parser.parse_args()

    if args.postal_votes:
        checker.set_mode('postal')
    if args.reminder:
        checker.set_mode('reminder')
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
        checker.set_debug(True)

    voting_members, no_members, postal_voters = checker.get_frames()

    if not args.debug:
        if args.reminder:
            if args.postal_votes:
                send_reminder_mails(no_members[no_members['postal']])
            else:
                send_reminder_mails(no_members[~no_members['postal']])
        if args.postal_votes:
            send_postal_voter_mails(postal_voters)
        else:
            send_regular_mails(voting_members)
    else:
        # TODO: Implement feature to set debugging address
        print(voting_members)
        print(postal_voters)
        print(no_members)

    no_members.to_excel('data/no_members.xlsx', index=False)
