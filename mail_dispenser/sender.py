import os
import smtplib
import logging
from email.message import EmailMessage
import pandas as pd


class MailDispenser:

    def __init__(self, data: pd.DataFrame, path_to_text: str):

        # Set recipients and file for mail
        self.recipients = data
        self.content = get_text(path_to_text)

        # Load credentials
        try:
            self.password = os.environ['PASS']
            self.mail = os.environ['EMAIL']
            self.port = os.environ['PORT']
            self.server = os.environ['SERVER']
        except KeyError as error:
            logging.error('Some key missing in environment variables.')
        self.connection = self.connect_to_mail_server()

    def connect_to_mail_server(self):
        try:
            server_conn = smtplib.SMTP_SSL(self.server, int(self.port))
            server_conn.connect(self.server, int(self.port))
            server_conn.login(self.mail, self.password)
            logging.info('Connected to server successfully.')
            return server_conn

        except BaseException as exception:
            logging.exception(exception)

    def broadcast(self, subject: str = None):
        self.recipients = self.recipients.copy()
        if not self.recipients.empty:
            sent_massages = self.recipients.apply(send_emails, axis='columns', dispenser=self, subject=subject)
            self.recipients.loc[sent_massages, 'send'] = True
            self.recipients.loc[~sent_massages, 'send'] = False
        print(self.recipients)

def get_text(path: str) -> str:
    with open(path, 'r') as file:
        content = file.read()
    return content


def send_emails(row: pd.Series, dispenser: MailDispenser, subject: str) -> bool:
    msg = EmailMessage()

    parameters = row.to_dict()
    text = dispenser.content.format(**parameters)

    msg.set_content(text)

    msg['Subject'] = subject
    msg['From'] = dispenser.mail
    msg['To'] = row['E-Mail']

    try:
        dispenser.connection.send_message(msg)
        return True
    except BaseException as exception:
        logging.exception(exception)
        return False


