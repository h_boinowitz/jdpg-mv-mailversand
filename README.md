# jDPG MV Mailversand

Ein Python-Skript, um der jDPG-Wahlordnung entsprechend die Tokens für die Abstimmung über LimeSurvey an die Mitglieder der jDPG zu versenden.

## Installation
### Voraussetzungen
- `pip`
- Python 3.x
### Anleitung
1. Clonen des Repositories
    ```
    git clone 
    ```
2. Installation der Pakete in `requirements.txt` entweder direkt im System oder in einer virtuellen Umgebung (eine Anleitung 
dazu findet sich [hier](https://docs.python.org/3/library/venv.html)) mit
    ```bash
   pip install -r requirements.txt 
   ```

## Anleitung
### Hinzufügen der Tabellen
Die Tabellen müssen, wie in diesem Repository unter `data` abegelegt werden.

- Tabellen können in der Formaten CSV, JSON und als Excel-Tabellen abgelegt werden
- die Namen der Tabellen sind dabei so zu wählen wie in diesem Repository
- die Tabelle mit den Online-Anmeldungen (`registrations_online`) ist dabei optional
- Die Spaltenbezeichnungen für die Spalten mit den Namen, Vornamen, der Mitgliedsnummer und der E-Mailadresse sind beizubehalten, alle anderen Spalten werden nciht verwendet.

### Tokens
Es kann eine beliebige Zahl Tokens in der ``tokens``-Tabelle eingefügt werden. Die Tokens werden vor dem Versenden immer gemischt, 
so dass aus der Anmelde- und Tokenliste keine Verbidnung zwischen Mitglied und Wahltoken hergestellt werden kann. 

### Mail-Texte
Die Mailtexte liegen im `text_files`-Ordner. Es wird, wie in den Beispieltexten abgespecktes Jinja-Templating benutzt,
um den Text zu personalisieren. Links können ganz normal mit in den Text aufgenommen werden, auch das Templating funktioniert dort identisch.


### Zugangsdaten für das Postfach, aus dem gesendet werden soll
Die Zugangsdaten sind als Umgebungsvariablen implementiert. 
Daher müssen diese in der lokalen `.bashrc` Datei abgelegt werden.

Wenn vorher noch keine ``.bashrc`` exisitierte, mit
```bash
touch .bashrc
```
die Datei erstellen.

In diese müssen dafür die folgenden Zeilen zum anlegen der Umgebungsvariablen eingefügt werden.

```bash
 export PASS="{YOUR PASSWORD}"
 export EMAIL="{YOUR EMAIL}"
 export PORT={YOUR PORT}
 export SERVER="{YOUR SERVER}"
```
Unter Linux(artigen)-Systemen dafür
```bash
cd ~
vim .bashrc
```

Ist die Datei entsprechend mit den Variablen gefüllt, 
```bash
source .bashrc
```
ausführen. Nach dem Senden kann die Datei wieder geleert werden. Mit einem führenden Leerzeichen in der `.bashrc` werden
die Variablen auch lokal nicht gecached.


### Ausführen

Das Paket kann über die Komandozeile mit
```bash
cd jdpg-mv-mailversand
python3 -m mail_dispenser
```
ausgeführt werden.

Zwei Flaggen stehen zur Verfügung:
- `--postal` kann verwendet werden, um nur die Briefwahlunterlagen zu versenden. 
- `--reminder` wird verwendet, um an alle, die in der Tabelle ``no_members`` stehen eine Mail mit dem 
Text aus ``text_files/aufforderung_mitgliedschaft.txt`` zu verschicken.

## Support
Alle Fragen zum Paket können an [pymailer@web.de](mailto:pymailer@web.de) gerichtet werden.
