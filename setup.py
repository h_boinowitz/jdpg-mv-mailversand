from setuptools import setup

setup(
    name='jdpg-mv-mailversand',
    packages=['mail_dispenser'],
    author='h_boinowitz',
    author_email='pymailer@web.de',
    description='Python-Paket zum Versand von E-Mails mit Zugangscodes für die Wahl auf der jDPG-Mitgliederversammlung'
)
